from os import listdir
from os.path import isfile, join, splitext
import matplotlib.pyplot as plt
import numpy as np

plt.xlabel("$i$ - lattice site")
plt.ylabel("$\\langle n_i \\rangle$ - density")
plt.ylim((0,1))


mypath="./"
onlyfiles = sorted([f for f in listdir(mypath) if isfile(join(mypath, f)) and "V_" in f and f.endswith(".dat")])

#print("These files will be plotted", sorted(onlyfiles))



for f in onlyfiles:
    print("plotting file ", f)
    name = splitext(f)[0]
    interaction=name.split("_")[1]
    densities = np.loadtxt(f)
    sites = np.arange(len(densities)) + 1
    plt.plot(sites,densities, 'o-', label="$V=" + interaction + "J$")

plt.legend()
plt.show()


#plt.savefig("density.pdf")
exit()