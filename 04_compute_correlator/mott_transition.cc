#include <iostream>
#include "itensor/all.h"
#include <complex>
#include <cmath>
#include "find_gs.h"
#include <vector>
using namespace itensor;
using namespace std;



int main()
    {
    // number of lattice sites
    int N = 40;

    // Creates a SiteSet for fermions consiting in a lattice of N sites 
    auto sites = Fermion(N);

    // Define the interaction strength
    double V = 0.10;

    // Compute the ground state psi using DMRG
    // the function computeGroundState is defined in find_gs.h
    auto psi = computeGroundState(sites, V);

    //define an empty vector for storing the densities 
    // <psi| n_j |psi>  for each site j
    auto density = vector<double>(N);


    // loop over all the lattice sites from 1 to N
    for (int j:range1(N))
    { 
        // ...
        // gauge the ground state psi to be at the site j
        // this means that 
        // the tensors at the left  of j will be left-orthogonal 
        // the tensors at the right of j will be right-orthogonal             
        
        // ...
        
        
        // define the number operator acting on site j 
        // auto n_op = ... 
        
        
        // Define n_psi as the contraction of n_op and psi(j)
        // auto n_psi = ...
        
        
        // Contract the dag(psi(j)) with n_psi
        // Remember to prime the exact indices in dag(psi(j))
        // auto psi_n_psi = ...
        
        
        // Exctract the value of the density from psi_n_psi 
        // with the function elt() 
        
        
        // print the value or
        // put it in the vector density with
        // density [j-1] = value // c++ starts counting the elements from 0
        
        
        
    }


    //save the content of the vector density to file
    char fname[20];
    sprintf(fname, "V_%1.3f.dat", V);
    saveCorrelator(density, fname);

    return 0;        
        
    }