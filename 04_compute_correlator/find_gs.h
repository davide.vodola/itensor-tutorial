#include "itensor/all.h"
#include <fstream>
#include <vector>
#include <iomanip>

using namespace std;
using namespace itensor;

MPS computeGroundState(Fermion const& sites,  double V)
    {

    auto L = length(sites);
    auto ampo = AutoMPO(sites);
    for(int i = 1; i < L; ++i) 
        {
        ampo += -1.0,"Cdag",i,  "C",   i+1;
        ampo += -1.0,"Cdag",i+1,"C",   i; 
        ampo += V,"N",i,"N",i+1;
        ampo += -V,"N",i;
        }
    
    ampo += -V,"N",L;
    
    auto H =  toMPO(ampo);

    auto sweeps = Sweeps(15);
    sweeps.maxdim() = 50,50,100,100,200;
    sweeps.cutoff() = 1E-10;
    
        
    InitState init_state(sites,"Emp");
    for (int j=1; j<=L; j++)
    {
        if (j%2==0) 
        { init_state.set(j,"Occ"); }
    }

     auto psi_trial = MPS(init_state);
     
     println("Starting ground state calculation for V = ",V);

    auto  [energy, psi] = dmrg(H, psi_trial, sweeps, "Quiet");

    println("Done with ground state calculation.");
        
    
    return psi;
    }



void saveCorrelator(const vector<double> corr, const string file_name)
{

    int prec = 8;
    int wid = prec + 10;
    ofstream out_file(file_name);
    
    for (int i = 0; i<corr.size(); i++)
    {
          out_file.setf(ios::showpos);
          out_file.setf(ios::scientific);
          out_file.setf(ios::left);
          out_file.fill(' ');
          out_file.precision(prec);
          out_file.width(wid);
          out_file << corr[i] << endl;
    }
    
}
