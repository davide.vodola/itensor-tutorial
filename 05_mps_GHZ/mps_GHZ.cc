#include "itensor/all.h"

using namespace itensor;

using namespace std;

int main(int argc, char * argv[] )
{   

    int num_sites = 3;
    int local_dim = 2;
    
    // Define 3 indices with dimension 2
    SiteSet sites(num_sites, local_dim);

    
    // Define the GHZ state as an Itensor with 3 indices
    ITensor ghz(sites(1), sites(2), sites(3));

    // Fill ghz with with psi = |111> + |222>
    ghz.set(sites(1) = 1,sites(2) = 1, sites(3) = 1, 1.);
    ghz.set(sites(1) = 2,sites(2) = 2, sites(3) = 2, 1.);    
    
    PrintData(ghz);

    //Define an empty MSP with MPS(sites)
    MPS ghz_mps=MPS(sites);


    //Define 3 tensors A1, D1, V1 with the right indices 
    // .......
    
    // perform the SVD    
    // .......
    
    // Copy A1 in ghz_mps
    // .......    
    
    // Contract D1 and V1. Call the new tensor psi2
    // .......
    
    
    //Extract the commond index l1 between A1 and D1
    // .......
    
    
    //Define 3 tensors A2, D2, V2 with the right indices 
    //A2 will need two indices l1 and sites(2)
    //......
   
    // perform the SVD of psi2 
    // .......  

    // Copy A2 in ghz_mps 
    // .......
    
    //Contract D2 and V2. Call the new tensor psi3
        
    // Copy psi3 in ghz_mps 
    // .......

    //Print the ghz as MPS
   PrintData(ghz_mps);  

}
